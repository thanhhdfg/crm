package com.globits.da.repository;

import com.globits.da.domain.Combo;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ComboRepository extends JpaRepository<Combo, Long> {


}
