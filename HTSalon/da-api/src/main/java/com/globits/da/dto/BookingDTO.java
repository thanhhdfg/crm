package com.globits.da.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.globits.core.dto.BaseObjectDto;

import javax.persistence.Column;
import java.time.LocalDateTime;

public class BookingDTO extends AbstractBaseDTO {
    @Column(name = "user_id")
    private String userId;

    //(pending/ok/cancel | 0/1/2)
    @Column (name = "status")
    private String status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Column (name = "date_time")
    private LocalDateTime date_time;

    @Column (name = "other_required")
    private String otherRequired;

    //(off/on | 0/1)
    @Column (name = "take_photo")
    private Integer takePhoto;

    @Column (name = "photo")
    private String photo;

    public BookingDTO(){
        super();
    }

    public BookingDTO(String userId, String status, LocalDateTime date_time, String otherRequired, Integer takePhoto, String photo) {
        this.userId = userId;
        this.status = status;
        this.date_time = date_time;
        this.otherRequired = otherRequired;
        this.takePhoto = takePhoto;
        this.photo = photo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDate_time() {
        return date_time;
    }

    public void setDate_time(LocalDateTime date_time) {
        this.date_time = date_time;
    }

    public String getOtherRequired() {
        return otherRequired;
    }

    public void setOtherRequired(String otherRequired) {
        this.otherRequired = otherRequired;
    }

    public Integer getTakePhoto() {
        return takePhoto;
    }

    public void setTakePhoto(Integer takePhoto) {
        this.takePhoto = takePhoto;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
