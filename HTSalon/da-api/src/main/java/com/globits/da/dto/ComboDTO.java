package com.globits.da.dto;

import com.globits.core.dto.BaseObjectDto;
import com.globits.da.domain.Combo;

import javax.persistence.Column;

public class ComboDTO extends AbstractBaseDTO {
    private String name;
    private Long price;
    private  Integer status;

    public ComboDTO(){
        super();
    }

    public ComboDTO(String name, Long price, Integer status) {
        this.name = name;
        this.price = price;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
