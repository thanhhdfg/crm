package com.globits.da.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.SQLDelete;

@Entity
@Table(name = "tbl_service")
@XmlRootElement
public class Service extends AbstractAuditingEntity{

	@Column(name = "name")
	private String name;

	//(dichvu/kieutoc 0/1)
	@Column(name = "type")
	private Integer type;

	@Column(name = "price")
	private Long price;

	//(off/on | 0/1)
	@Column(name = "status")
	private Integer status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
