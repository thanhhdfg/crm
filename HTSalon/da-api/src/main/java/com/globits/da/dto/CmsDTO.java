package com.globits.da.dto;

import com.globits.core.dto.BaseObjectDto;
import com.globits.da.domain.Cms;

import javax.persistence.Column;

public class CmsDTO extends AbstractBaseDTO {
    private Long comboId;
    private Long serviceId;
    private Integer status;

    public CmsDTO(){
        super();
    }

    public CmsDTO(Long comboId, Long serviceId, Integer status) {
        this.comboId = comboId;
        this.serviceId = serviceId;
        this.status = status;
    }

    public Long getComboId() {
        return comboId;
    }

    public void setComboId(Long comboId) {
        this.comboId = comboId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
