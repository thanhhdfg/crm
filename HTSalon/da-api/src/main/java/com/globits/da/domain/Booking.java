package com.globits.da.domain;

import org.hibernate.annotations.SQLDelete;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

@Entity
@Table(name = "tbl_booking")
@XmlRootElement
public class Booking extends AbstractAuditingEntity {
    @Column (name = "user_id")
    private String userId;

    //(pending/ok/cancel | 0/1/2)
    @Column (name = "status")
    private String status;

    @Column (name = "date_time")
    private LocalDateTime date_time;

    @Column (name = "other_required")
    private String otherRequired;

    //(off/on | 0/1)
    @Column (name = "take_photo")
    private Integer takePhoto;

    @Column (name = "photo")
    private String photo;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDate_time() {
        return date_time;
    }

    public void setDate_time(LocalDateTime date_time) {
        this.date_time = date_time;
    }

    public String getOtherRequired() {
        return otherRequired;
    }

    public void setOtherRequired(String otherRequired) {
        this.otherRequired = otherRequired;
    }

    public Integer getTakePhoto() {
        return takePhoto;
    }

    public void setTakePhoto(Integer takePhoto) {
        this.takePhoto = takePhoto;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
