package com.globits.da.repository;

import com.globits.da.domain.Cms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CmsRespository extends JpaRepository<Cms, Long> {

}
