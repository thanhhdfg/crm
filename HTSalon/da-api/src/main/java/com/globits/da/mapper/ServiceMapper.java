package com.globits.da.mapper;

import com.globits.da.domain.Service;
import com.globits.da.dto.ServiceDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ServiceMapper extends EntityMapper<ServiceDTO, Service> {
}
