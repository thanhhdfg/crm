package com.globits.da.domain;

import org.hibernate.annotations.SQLDelete;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "tbl_cms")
@XmlRootElement
public class Cms extends AbstractAuditingEntity {
    @Column(name = "combo_id")
    private Long comboId;
    @Column(name = "service_id")
    private Long serviceId;
    @Column(name = "status")
    private Integer status;

    public Cms() {
    }

    public Cms(Long comboId, Long serviceId, Integer status) {
        this.comboId = comboId;
        this.serviceId = serviceId;
        this.status = status;
    }

    public Long getComboId() {
        return comboId;
    }

    public void setComboId(Long comboId) {
        this.comboId = comboId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
