package com.globits.da.repository;

import com.globits.da.domain.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
    public interface ServiceRepository extends JpaRepository<Service, Long> {

    }
