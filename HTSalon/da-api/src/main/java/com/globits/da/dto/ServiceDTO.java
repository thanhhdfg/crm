package com.globits.da.dto;

public class ServiceDTO extends AbstractBaseDTO {

    private String name;
    //(dichvu/kieutoc 0/1)
    private Integer type;
    private Long price;
    //(off/on | 0/1)
    private Integer status;

    public ServiceDTO(){
        super();
    }

    public ServiceDTO(String name, Integer type, Long price, Integer status) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
